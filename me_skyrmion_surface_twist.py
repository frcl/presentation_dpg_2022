#pylint: ignore=too-few-public-methods
import numpy as np
from manim import *
from manim_editor import PresentationSectionType


class MESurfaceTwist3D(Scene):

    def construct(self):

        lagrangian = VGroup()
        lagrangian += MathTex(r"""
            \mathcal{L}
                = \frac{J}{2} (\partial_i m_j)^2
                    + D \mathbf{m} \cdot (\nabla \times \mathbf{m})
                    &+ K (\mathbf{m} \cdot \hat{\mathbf{u}})^2
                    - \mathbf{h} \cdot \mathbf{m}
            """
        ).scale(0.8).move_to(2*DOWN)
        lagrangian += MathTex(
            r'+\;\varepsilon_{ij} \lambda_{ijkl} m_k m_l',
            r'+\frac12 \varepsilon_{ij} C_{ijkl} \varepsilon_{kl}',
        ).scale(0.8).move_to(3*DOWN)

        self.add(lagrangian)
        self.wait()

        self.play(lagrangian.animate.scale(0.6).move_to(RIGHT*3.5 + UP*3))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)

        parameters = MathTex(
            r'''\begin{aligned}
                J &= 1.0 \\
                D &= 1.0 \\
                K &= 0.5 \\
                h &= -0.5 \\
                C_{11} &= 11\cdot 10^6 \\
                C_{12} &= 4.8\cdot 10^6 \\
                C_{44} &= 4.4\cdot 10^6 \\
                \lambda_{11} &= 2.4\cdot 10^3 \\
                \lambda_{44} &= 1.8\cdot 10^3
            \end{aligned}'''
        ).scale(0.4).move_to(LEFT*6)

        self.play(Write(parameters))
        self.wait()


class S080_MEvsNoME(Scene):

    def construct(self):
        self.next_section(type=PresentationSectionType.NORMAL)

        ax = Axes(
            x_range=[-2, 2, 1],
            y_range=[90, 180, 15],
            x_length=8,
            y_length=5,
            tips=False,
            axis_config={"include_numbers": True},
            x_axis_config={"scaling": LogBase(custom_labels=True)},
        )
        theory_plot = VGroup(ax)
        theory_plot += ax.get_x_axis_label('z / Q')
        theory_plot += ax.get_y_axis_label(r'\chi')
        theory_plot += ax.get_horizontal_line(ax.c2p(100, 180, 0), line_func=Line)
        theory_plot += ax.get_vertical_line(ax.c2p(100, 180, 0), line_func=Line)
        self.play(Create(theory_plot))

        x0 = -1.2
        th_line = ax.plot(
            lambda x: 4*np.arctan(np.exp(-np.sqrt(2)*(x-x0)))/(2*np.pi)*360 + 90,
            x_range=[-2, 2],
        ).set_color(ORANGE)

        theory_plot.add(th_line)
        self.play(Create(th_line))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)
        z, chi = np.loadtxt(
            '/home/lars/tmp/fenics_data/me_skyrmion_lattice_3184/helicity.csv',
            delimiter=',', unpack=True,
        )
        sim_line = ax.plot_line_graph(z, chi/(2*np.pi)*360+90, add_vertex_dots=False).set_color(BLUE)

        self.next_section(type=PresentationSectionType.NORMAL)
        theory_plot.add(sim_line)
        self.play(Create(sim_line))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)
        length = MathTex(r'L_{me} \approx 1.22/Q').move_to(1*UP + 2*RIGHT)
        theory_plot.add(length)
        self.play(Write(length))
        self.wait()
