import numpy as np
from manim import *
from manim_editor import PresentationSectionType


def theta(r, radius):
    return 2*np.arctan(1/(r/2+0.001)**2)


def skymion_wrapper(helicity, radius=1.0):
    def skymion_func(pos):
        r = np.linalg.norm(pos)
        phi = np.arctan2(pos[1], pos[0])
        # return np.array([1., 0., pos[0]])
        return np.array([np.sin(theta(r, radius))*np.cos(phi+helicity),
                         np.sin(theta(r, radius))*np.sin(phi+helicity),
                         np.cos(theta(r, radius))])

    return skymion_func


class S000_Neel2Bloch(Scene):
    def construct(self):
        self.next_section(type=PresentationSectionType.LOOP)

        x_range = (-7, 7, 1)

        n1 = ArrowVectorField(skymion_wrapper(0), x_range=list(x_range))
        b1 = ArrowVectorField(skymion_wrapper(np.pi/2), x_range=list(x_range))
        n2 = ArrowVectorField(skymion_wrapper(np.pi), x_range=list(x_range))
        b2 = ArrowVectorField(skymion_wrapper(3*np.pi/2), x_range=list(x_range))

        m = n1.copy()

        title_text = Tex(r"Magnetoelastic surface states\\of skyrmion textures", font_size=72).to_edge(UP)
        author_text = Tex("Lars Franke, KIT", font_size=36).to_edge(DOWN)

        self.add(m, title_text, author_text)
        self.wait()
        self.play(m.animate.become(b1))
        self.wait()
        self.play(m.animate.become(n2))
        self.wait()
        self.play(m.animate.become(b2))
        self.wait()
        self.play(m.animate.become(n1))
        self.wait()


class S090_Outlook(Scene):

    def construct(self):
        self.next_section(type=PresentationSectionType.NORMAL)

        title_text = Tex("Outlook", font_size=72).to_edge(UP)
        outlook_points = BulletedList(
            r'Magnetoelastic coupling can extend length scale of surface twist,\\'
            r'but only for unrealistically high magnetoelastic coupling',
            r'$\mathcal{L}_\mathrm{int} = \varepsilon_{ij} \mu_{ijkl} \epsilon_{kmn} m_m \partial_l m_n$\\'
            r'\hspace{1em}or $(\nabla \times \mathbf{u})_i \epsilon_{ijk} \epsilon_{jln} m_l \partial_k m_n$',
            r'Dynamics?',
            font_size=36,
        )
        contact_text = Tex(r"email: lars.franke@kit.edu\\matrix: @lars.franke:kit.edu",
                           font_size=36).to_edge(DOWN)

        self.play(FadeIn(title_text))
        self.wait()
        for item in outlook_points:
            self.next_section(type=PresentationSectionType.NORMAL)
            self.play(FadeIn(item))
            self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeIn(contact_text))
        self.wait()
