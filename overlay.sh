scene=$1
overlay=$2
output=$3

# get last frame as an image
ffmpeg -y -sseof -3 -i "$scene" -update 1 -q:v 1 background.png

# 700 is appropriate for low quality
# 2100 for production (2560x1440)
#quality=700
quality=2250

ffmpeg -y -loop 1 -i background.png -i "$overlay" -filter_complex \
"[1:v]scale=$quality:-1,fade=in:st=0:d=2:alpha=1,fade=out:st=8:d=2:alpha=1[a]; \
 [0:v][a]overlay=main_w-overlay_w:main_h-overlay_h:shortest=1[video]" \
-map "[video]" "$output"
