import numpy as np

center = (6.283627018332481, 7.25718080997467)

r = np.sqrt((-1-center[0])**2+(15-center[1])**2)

dphi = 5.5283

for phi in np.linspace(dphi,2*np.pi+dphi, 8, endpoint=False):
    print(center[0]+r*np.sin(phi), center[1]+r*np.cos(phi))
