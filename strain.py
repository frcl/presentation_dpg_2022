#pylint: ignore=too-few-public-methods
import numpy as np
from manim import *
from manim_editor import PresentationSectionType


def normal_strain(scene):

    center = 4.5*LEFT + UP

    var_e_xx = Variable(0., r'\varepsilon_{xx}', num_decimal_places=2)
    var_e_yy = Variable(0., r'\varepsilon_{yy}', num_decimal_places=2)
    e_vars = VGroup(var_e_xx, var_e_yy).arrange(DOWN).scale(0.6).move_to(center+3.5*RIGHT)

    e_xx = var_e_xx.tracker
    e_yy = var_e_yy.tracker
    distorted_lattice = VGroup()
    for x in np.arange(-1.2, 1.3, 0.6):
        for y in np.arange(-1.2, 1.3, 0.6):
            # e_xx and e_yy
            distorted_lattice += Dot(center+np.array([x, y, 0])) \
                .add_updater(lambda m, x=x: m.set_x(center[0]+x*(1+e_xx.get_value()))) \
                .add_updater(lambda m, y=y: m.set_y(center[1]+y*(1+e_yy.get_value())))

    larrow = Arrow(start=[0, 0, 0], end=RIGHT).move_to(center+2*LEFT)
    rarrow = Arrow(start=[0, 0, 0], end=LEFT).move_to(center+2*RIGHT)

    scene.next_section(type=PresentationSectionType.NORMAL)

    scene.play(Create(distorted_lattice), Create(e_vars))
    scene.wait()
    scene.next_section(type=PresentationSectionType.NORMAL)

    scene.play(Create(larrow), Create(rarrow))
    scene.play(e_xx.animate.set_value(-0.2),
               e_yy.animate.set_value(0.2))
    scene.wait()
    return Group(distorted_lattice, larrow, rarrow, e_vars)


def shear_strain(scene):

    center = 2.5*RIGHT + UP

    var_e_xy = Variable(0., r'\varepsilon_{xy}', num_decimal_places=2)
    var_e_xy.scale(0.6).move_to(center+3*RIGHT)

    e_xy = var_e_xy.tracker
    distorted_lattice = VGroup()
    for x in np.arange(-1.2, 1.3, 0.6):
        for y in np.arange(-1.2, 1.3, 0.6):
            # e_xy
            distorted_lattice += Dot(center+np.array([x, y, 0])) \
                .add_updater(lambda m, x=x, y=y: m.set_y(center[1]+y+x*e_xy.get_value()))

    larrow = Arrow(start=0.5*DOWN, end=0.5*UP).move_to(center+2*LEFT)
    rarrow = Arrow(start=0.5*UP, end=0.5*DOWN).move_to(center+2*RIGHT)

    scene.next_section(type=PresentationSectionType.NORMAL)

    scene.play(Create(distorted_lattice), Create(var_e_xy))
    scene.wait()

    scene.next_section(type=PresentationSectionType.NORMAL)

    scene.play(Create(larrow), Create(rarrow))
    scene.play(e_xy.animate.set_value(-0.2))
    scene.wait()
    return Group(distorted_lattice, larrow, rarrow, var_e_xy)


def normal_arrows(lattice):
    direct = np.array([0.4, 0., 0.])
    arrows = VGroup()
    for dot in lattice:
        arrows += Arrow(start=dot.get_center()-0.5*direct, end=dot.get_center()+0.5*direct).set_color(ORANGE)
    return arrows


def shear_arrows(lattice):
    direct = np.sqrt(0.4**2*0.5)*np.array([1., 1., 0.])
    arrows = VGroup()
    for dot in lattice:
        arrows += Arrow(start=dot.get_center()-0.5*direct, end=dot.get_center()+0.5*direct).set_color(ORANGE)
    return arrows


class S050_MECoupling(Scene):

    def construct(self):

        normal = normal_strain(self)
        shear = shear_strain(self)

        self.next_section(type=PresentationSectionType.NORMAL)
        narr = normal_arrows(normal[0])
        sarr = shear_arrows(shear[0])
        self.play(Create(narr), Create(sarr))

        lagrangian = MathTex(r"""\begin{aligned}
            \mathcal{L}
                = \frac{J}{2} (\partial_i m_j)^2
                    + D \mathbf{m} \cdot (\nabla \times \mathbf{m})
                    &+ K (\mathbf{m} \cdot \hat{\mathbf{u}})^2
                    - \mathbf{h} \cdot \mathbf{m}
            \end{aligned}"""
        ).scale(0.8).move_to(2*DOWN)
        extra_term = MathTex(
            r'+\;\varepsilon_{ij} \lambda_{ijkl} m_k m_l',
            r'+\frac12 \varepsilon_{ij} C_{ijkl} \varepsilon_{kl}',
        ).scale(0.8).move_to(3*DOWN)

        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(Write(lagrangian))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(Write(extra_term))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeOut(normal), FadeOut(shear), FadeOut(narr), FadeOut(sarr))
        self.wait()
