# Structure

* Intro: Why should we care?
    * Surfaces of chiral magentes
    * Already give an outline of the talk
* Probelm schwer: Skyrmion surface twist
* Idee neu: Magnetoelastic coupling
* Lösung gut: Perturbative calculation and simulations both show larger lengthscales
* Outro: Closing remarks
    * TODO: Punchline
    * maybe rant about the state of magnetoelastic coupling simulators

# Time frame

target = 12+3min

* intro and general talk about surface states and that it's important to understand them [30s]
* Show theory (Exchange + DMI + uniaxial + zeemann) [30s]
* Show video of simulation showing surface twist [1m]
* maybe calculation of soliton at surface and comparison to simulation [1-2m]
* compare to experiment and see it's wrong [1m]
* what can explain the discrepancy? idea: magnetoelastic coupling [30s]
* explain strain and why it changies magnetric interaction [30s]
* show first order term and explain why it makes sense from a symmetry/field theory perspective [30s]
* show video of simulation including displacement field/strain [1m]
* show graph of simulated helicity, see lengthscale is longer [30s]
* how can we understand this? look at equation of motion, extract lengthscale [1m]
* show perturbative expansion of L in chi [1m]
* close with some insightful remarks? [30s]
    * we can find larger length scale but for relatively large coupling
    * the effect might also be connected to other energy terms contributing: sym-chiral, antisym-chiral [30s]

sum = 10min 30s
