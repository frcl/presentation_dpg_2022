#common_options = -ql --save_sections --progress_bar none
common_options = -qp --save_sections --progress_bar none

slides:
	manim $(common_options) title_slide.py -a
	manim $(common_options) skyrmion_surface_twist_nome.py -a
	manim $(common_options) soliton.py -a
	manim $(common_options) strain.py -a
	manim $(common_options) me_skyrmion_surface_twist.py -a
	manim $(common_options) perturbative_scale.py -a

#res = 480p15
res = 1440p60

videos:
	mkdir -p media/videos/skyrmion_surface_twist_nome_sim/$(res)/sections/
	./overlay.sh \
		media/videos/skyrmion_surface_twist_nome/$(res)/SurfaceTwist3D.mp4 \
		~/tmp/fenics_data/nome_skyrmion_lattice_5520/nome_surface_twist.avi \
		media/videos/skyrmion_surface_twist_nome_sim/$(res)/SurfaceTwist3DSim.mp4
	cp media/videos/skyrmion_surface_twist_nome_sim/$(res)/SurfaceTwist3DSim.mp4 \
	    media/videos/skyrmion_surface_twist_nome_sim/$(res)/sections/SurfaceTwist3DSim.mp4
	mkdir -p media/videos/me_skyrmion_surface_twist_sim/$(res)/sections/
	./overlay.sh \
		media/videos/me_skyrmion_surface_twist/$(res)/MESurfaceTwist3D.mp4 \
		~/tmp/fenics_data/me_skyrmion_lattice_3184/surface_twist_me.avi \
		media/videos/me_skyrmion_surface_twist_sim/$(res)/MESurfaceTwist3DSim.mp4
	cp media/videos/me_skyrmion_surface_twist_sim/$(res)/MESurfaceTwist3DSim.mp4 \
	    media/videos/me_skyrmion_surface_twist_sim/$(res)/sections/MESurfaceTwist3DSim.mp4
	./overlay.sh \
		media/videos/me_skyrmion_surface_twist/$(res)/MESurfaceTwist3D.mp4 \
		~/tmp/fenics_data/me_skyrmion_lattice_3184/surface_profile_me.avi \
		media/videos/me_skyrmion_surface_twist_sim/$(res)/MESurfaceProfile3D.mp4
	cp media/videos/me_skyrmion_surface_twist_sim/$(res)/MESurfaceProfile3D.mp4 \
	    media/videos/me_skyrmion_surface_twist_sim/$(res)/sections/MESurfaceProfile3D.mp4
