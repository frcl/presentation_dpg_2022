#pylint: ignore=too-few-public-methods
import numpy as np
from manim import *
from manim_editor import PresentationSectionType


class SurfaceTwist3D(Scene):

    def construct(self):

        lagrangian = Group(*map(MathTex, (
            r'\mathcal{L}',
            r'= \frac{J}{2} (\partial_i m_j)^2',
            r'+ D \mathbf{m} \cdot (\nabla \times \mathbf{m})',
            r'+ K (\mathbf{m} \cdot \hat{\mathbf{u}})^2',
            r'- \mathbf{h} \cdot \mathbf{m}',
        )))
        lagrangian.arrange(RIGHT)

        for term in lagrangian:
            self.next_section(type=PresentationSectionType.NORMAL)
            self.play(Write(term))
            self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(lagrangian.animate.scale(0.5).move_to(RIGHT*4 + UP*3.5))
        self.wait()

        parameters = MathTex(
            r'''\begin{aligned}
                J &= 1.0 \\
                D &= 1.0 \\
                K &= 0.5 \\
                h &= -0.5
            \end{aligned}'''
        ).scale(0.4).move_to(LEFT*6)

        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(Write(parameters))
        self.wait()

        # self.next_section(type=PresentationSectionType.NORMAL)

        # q_def = MathTex(r'Q = \frac D J').move_to(LEFT*5 + UP*3)

        # self.play(Write(q_def))
        # self.wait()


class S020_SineGordon(Scene):

    def construct(self):
        self.next_section(type=PresentationSectionType.NORMAL)

        var_helicity = Variable(np.pi/2, r'\chi', num_decimal_places=0)
        helicity = var_helicity.tracker

        ring = VGroup()
        for phi in np.arange(0, 2*np.pi, np.pi/4):
            pos = 2*np.array([np.sin(phi), np.cos(phi), 0])
            dir = np.array([-np.cos(phi), np.sin(phi), 0])
            ring += Arrow(start=pos-0.5*dir, end=pos+0.5*dir, buff=0) \
                .add_updater(lambda m, phi=phi: m.set_angle(np.pi/2-phi+helicity.get_value()))

        bdir = ring[-1].start - ring[-1].end
        line_static = Line(
            start=ring[-1].start,
            end=ring[-1].start + np.array([-bdir[1], bdir[0], 0]),
        ).set_color(ORANGE)

        self.play(Create(ring))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(helicity.animate.set_value(np.pi/4))

        line_moving = line_static.copy()
        line_moving.set_angle(3*np.pi/4+helicity.get_value())
        arc = Angle(line_static, line_moving, radius=0.5, other_angle=False).set_color(ORANGE)
        arc_label = MathTex(r'\chi').set_color(ORANGE).scale(2).move_to(LEFT*2.5+UP*2)

        ring.add(line_static, line_moving, arc)
        self.play(Create(line_static), Create(line_moving), Create(arc))
        # self.play(Create(line_static))
        ring.add(arc_label)
        self.play(Write(arc_label))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(ring.animate.scale(0.4).move_to(LEFT*4.5 + UP*2))

        ansatz = MathTex(
            r'''\mathbf{m}(\mathbf{r})
                = \sum_{\mathbf{q}} b_\mathbf{q} \left[ \left(
                    \frac{\hat{\mathbf{q}}}{|\mathbf{q}|} \sin\chi(z)
                    + \hat{\mathbf{e}}_z \times \frac{\hat{\mathbf{q}}}{|\mathbf{q}|} \cos\chi(z)
                \right) \sin(\mathbf{q}\cdot\mathbf{r})
                + \hat{\mathbf{e}}_z \cos(\mathbf{q}\cdot\mathbf{r}) \right]'''
        ).scale(0.6).move_to(RIGHT*2 + UP*2)

        triple_q = Group(*[
            Arrow(start=ORIGIN, end=point, buff=0) for point in
            ([2, 0, 0], [-1, np.sqrt(3), 0], [-1, -np.sqrt(3), 0])
        ]).scale(0.5).move_to(LEFT*4.5 + UP*2)

        action_generic = MathTex(r'S = \int d^3r \;\mathcal{L}').move_to(DOWN)

        action_chiral_magnet = MathTex(
            r'S = \int d^3r',
            r'\Big(',
            r'\frac{J}{2} (\partial_i m_j)^2',
            r'+ D \mathbf{m} \cdot (\nabla \times \mathbf{m})',
            r'+ K (\mathbf{m} \cdot \hat{\mathbf{u}})^2',
            r'- \mathbf{h} \cdot \mathbf{m}',
            r'\Big)',
        ).move_to(DOWN)

        action_helicity = MathTex(
            r'S = \int dz',
            r'\Big(',
            r"\frac12 (\chi')^2",
            r"- \chi'",
            r'- 2Q^2 \cos\chi',
            r'\Big)',
            r'+ \text{const.}',
        ).move_to(DOWN)

        q_def = MathTex(r'Q = \frac D J').move_to(RIGHT*3 + DOWN*3)

        sine_grodon_equation = MathTex(
            r"\chi'' =",
            r'2Q^2',
            r'\sin\chi',
        ).move_to(2*LEFT + 3*UP)
        boundary_condition = MathTex(
            r"\chi'|_\mathcal{S} = -1"
        ).move_to(2*RIGHT + 3*UP)

        self.play(Write(ansatz))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeOut(ring))
        self.play(*map(Create, triple_q))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)

        action = action_generic.copy()
        self.play(Write(action))
        self.wait()
        self.play(action.animate.become(action_chiral_magnet))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(action.animate.become(action_helicity),
                  FadeOut(ansatz, target_position=action_chiral_magnet, scale=0.5),
                  FadeOut(triple_q))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(Write(q_def))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeOut(action), FadeOut(q_def),
                  FadeIn(sine_grodon_equation, target_position=action),
                  FadeIn(boundary_condition, target_position=action))


class S040_Experiment(Scene):

    def construct(self):
        self.next_section(type=PresentationSectionType.NORMAL)

        ax = Axes(
            x_range=[-2, 2, 1],
            y_range=[90, 180, 15],
            x_length=8,
            y_length=5,
            tips=False,
            axis_config={"include_numbers": True},
            x_axis_config={"scaling": LogBase(custom_labels=True)},
        )
        theory_plot = VGroup(ax)
        theory_plot += ax.get_x_axis_label('z / Q')
        theory_plot += ax.get_y_axis_label(r'\chi')
        theory_plot += ax.get_horizontal_line(ax.c2p(100, 180, 0), line_func=Line)
        theory_plot += ax.get_vertical_line(ax.c2p(100, 180, 0), line_func=Line)
        self.play(Create(theory_plot))

        x0 = -1.2
        th_line = ax.plot(
            lambda x: 4*np.arctan(np.exp(-np.sqrt(2)*(x-x0)))/(2*np.pi)*360 + 90,
            x_range=[-2, 2],
        ).set_color(ORANGE)

        theory_plot.add(th_line)
        self.play(Create(th_line))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)

        length = MathTex(r'L_\mathrm{th} = \frac{1}{\sqrt{2}Q}').move_to(1*UP + 2*RIGHT)
        theory_plot.add(length)
        self.play(Write(length))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)

        z, chi = np.loadtxt(
            '/home/lars/tmp/fenics_data/nome_skyrmion_lattice_5520/helicity.csv',
            delimiter=',', unpack=True,
        )
        sim_line = ax.plot_line_graph(z, chi/(2*np.pi)*360+90, add_vertex_dots=False).set_color(BLUE)

        theory_plot.add(sim_line)
        self.play(Create(sim_line))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(theory_plot.animate.scale(0.65).move_to(3*LEFT))

        plot = SVGMobject(
            '/home/lars/phd/presentation_dpg_2022fall/figures/helicity_plot_exp.svg',
            stroke_width=2,
        ).scale(2.).move_to(3*RIGHT + 0.3*DOWN)

        citation = Tex('[Zhang et al. PNAS 2018]').scale(0.4).move_to(2.5*DOWN + 5*RIGHT)

        self.play(FadeIn(plot))
        self.play(FadeIn(citation))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeOut(theory_plot), FadeOut(plot), FadeOut(citation))
        self.wait()
