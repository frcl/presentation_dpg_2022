import numpy as np
from manim import *
from manim_editor import PresentationSectionType


def kink(x, t=1):
    theta = t*4*np.arctan(np.exp(x/2))
    return np.array([0, -np.sin(theta), np.cos(theta)])


class S030_Kink(ThreeDScene):


    def construct(self):
        self.next_section(type=PresentationSectionType.NORMAL)

        phi, theta, focal_distance, gamma, distance_to_origin = self.camera.get_value_trackers()
        phi.set_value(90*DEGREES)

        sine_grodon_equation = MathTex(
            r"\chi'' =",
            r'2Q^2',
            r'\sin\chi',
        ).move_to(2*LEFT + 3*UP + 2*IN).rotate_about_origin(90*DEGREES, RIGHT)
        boundary_condition_in_xy = MathTex(
            r"\chi'|_\mathcal{S} = -1"
        )
        boundary_condition_rect = SurroundingRectangle(boundary_condition_in_xy, buff = .1) \
            .move_to(2*RIGHT + 3*UP + 2*IN) \
            .rotate_about_origin(90*DEGREES, RIGHT) \
            .set_color(ORANGE)
        boundary_condition = boundary_condition_in_xy \
            .move_to(2*RIGHT + 3*UP + 2*IN) \
            .rotate_about_origin(90*DEGREES, RIGHT)

        self.add(sine_grodon_equation, boundary_condition)
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)

        soliton_solution = MathTex(
            r'\chi(z) = 4 \arctan\left(e^{\sqrt{2Q}(z-z_0)}\right)'
        ).move_to(UP + 2*IN).rotate_about_origin(90*DEGREES, RIGHT)
        self.play(Write(soliton_solution))

        x0 = ValueTracker(0.)

        chain = VGroup()
        for x in np.arange(-6, 6, 0.4):
            point = np.array([x, 0, -1])
            chain += Arrow(start=point, end=point+kink(x)) \
                .add_updater(lambda m, x=x:
                    m.put_start_and_end_on(np.array([x, 0, -1]), np.array([x, 0, -1])+kink(x+x0.get_value()))
                     .set_color(interpolate_color(RED, BLUE, 0.5*(1+kink(x+x0.get_value())[2]))))


        x0.set_value(0.)
        self.play(Create(chain))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(
            phi.animate.set_value(60*DEGREES),
            theta.animate.set_value(-50*DEGREES),
        )
        self.wait()

        surface = Rectangle(width=3.0, height=3.0).rotate_about_origin(90*DEGREES, UP).move_to(6*LEFT+IN)
        surface_label = Text('Surface') \
            .rotate_about_origin(90*DEGREES, UP) \
            .rotate_about_origin(90*DEGREES, RIGHT) \
            .move_to(6*LEFT)
        self.play(Create(surface))
        self.play(Write(surface_label))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(x0.animate.set_value(6.5))
        self.play(Create(boundary_condition_rect))
        self.wait()
